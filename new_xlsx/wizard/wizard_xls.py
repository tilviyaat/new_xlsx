# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import xlwt
import base64
from io import StringIO
from odoo import api, fields, models, _
import platform
from openerp.exceptions import ValidationError


class XlsxReportOut(models.Model):
    _name = 'xlsx.report.out'

    file_data = fields.Char('Name', size=256)
    file_name = fields.Binary('Purchase Excel Report', readonly=True)


class WizardWizards(models.Model):
    _name = 'wizard.reports'

    start_date = fields.Datetime(string="Start Date", default=fields.Date.today(), required=True)
    end_date = fields.Datetime(string="End Date", default=fields.Date.today(), required=True)

    @api.constrains('end_date', 'start_date')
    def date_constrains(self):

        for rec in self:

            if rec.end_date < rec.start_date:
                raise ValidationError(_('Sorry, End Date Must be greater Than Start Date...'))


    def action_sample_report(self):
        label_lists=['PO','POR','CLIENTREF','BARCODE','DEFAULTCODE','NAME','QTY','VENDORPRODUCTCODE','TITLE', 'PARTNERNAME', 'EMAIL', 'PHONE', 'MOBILE', 'STREET', 'STREET2', 'ZIP', 'CITY', 'COUNTRY']

        workbook = xlwt.Workbook()
        style0 = xlwt.easyxf('font: name Times New Roman bold on;borders:left thin, right thin, top thin, bottom thin;align: horiz right;', num_format_str='#,##0.00')
        style2 = xlwt.easyxf('font:height 400,bold True;borders:left thick, right thick, top thick, bottom thick;', num_format_str='#,##0.00')
        style3 = xlwt.easyxf('font:bold True;borders:left thick, right thick, top thick, bottom thick;align: horiz center;', num_format_str='#,##0.00')
        style8 = xlwt.easyxf('font: name Times New Roman bold on;borders:left thick, right thick, top thick, bottom thick;align: horiz center;', num_format_str='DD-MM-YYYY')

        # sheet = workbook.add_sheet('Sample Excel Sheet')
        #
        #
        # sheet.write_merge(4, 4, 1, 1, 'Start Date', style3)
        # sheet.write_merge(4, 4, 2, 2, self.start_date, style8)
        # sheet.write_merge(5, 5, 8, 8, 'End Date', style3)
        # sheet.write_merge(5, 5, 9, 9, self.end_date, style0)
        sheet = workbook.add_sheet('Sale Orders')

        sheet.write_merge(6, 8, 7, 11, 'Sale Orders:', style2)
        sheet.write_merge(6, 8, 2, 3, 'Start Date', style3)
        sheet.write_merge(6, 8, 4, 6, self.start_date, style8)
        sheet.write_merge(6, 8, 13, 13, 'End Date', style3)
        sheet.write_merge(6, 8, 12, 12, self.end_date, style8)

        sheet.write_merge(9, 10, 2, 2, 'Number', style3)
        sheet.write_merge(9, 10, 3, 4, 'Creation Date', style3)
        sheet.write_merge(9, 10, 5, 6, 'Customer', style3)
        sheet.write_merge(9, 10, 7, 8, 'Sales Person', style3)
        sheet.write_merge(9, 10, 9, 10, 'Company', style3)
        sheet.write_merge(9, 10, 11, 11, 'Total', style3)
        sheet.write_merge(9, 10, 12, 13, 'Status', style3)

        j = 11
        obj = self.env['sale.order'].search([])
        for i in obj:
            if i.date_order >= self.start_date and i.date_order <= self.end_date:
                sheet.write_merge(j,j, 2,2, i.name, style8)
                sheet.write_merge(j,j, 3,4, i.create_date, style8)
                sheet.write_merge(j,j, 5,6, i.partner_id.name, style8)
                sheet.write_merge(j,j, 7,8, i.user_id.name, style8)
                sheet.write_merge(j,j, 9,10, i.company_id.name, style8)
                sheet.write_merge(j,j, 11,11, i.amount_total, style8)
                sheet.write_merge(j,j, 12,13, i.state, style8)

                j = j + 1

        output = StringIO()
        label = ';'.join(label_lists)
        output.write(label)
        output.write("\n")

        data = base64.b64encode(bytes(output.getvalue(),"utf-8"))

        if platform.system() == 'Linux':
            filename = ('/tmp/Excel Report' +'.xls')
        else:
            filename = ('Excel Report' +'.xls')

        workbook.save(filename)
        fp = open(filename, "rb")
        file_data = fp.read()
        out = base64.encodestring(file_data)

        # Files actions
        attach_vals = {
                'file_data': 'Purchase Report'+ '.xls',
                'file_name': out,
            }

        act_id = self.env['xlsx.report.out'].create(attach_vals)
        fp.close()
        return {
        'type': 'ir.actions.act_window',
        'res_model': 'xlsx.report.out',
        'res_id': act_id.id,
        'view_type': 'form',
        'view_mode': 'form',
        'context': self.env.context,
        'target': 'new',
        }































